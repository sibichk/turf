
angular.module('myApp', ['angular-responsive']);
var app = angular.module('myApp', []);
var animate = angular.module('animateModule', ['ngAnimate']);
app.controller('indexCtrl', function($scope) {

});
/* animation controller  */
app.controller('animateCtrl', function($scope){
    $scope.showSearchBox = function() {
       $.when($("div#header > div.menu").fadeOut(1000)).done(function() {$(".searchBox").fadeIn(1000)});
    }
    
    $scope.hideSearchBox = function(){
    	$('input[name="search"]').val(''); 
    	$.when($(".searchBox").fadeOut(1000)).done(function() {$("div#header > div.menu").fadeIn(1000)});
    }
    
    $('input[name="search"]').focus(
    	    function(){
    	        $(this).val('');
    	    });
 });

